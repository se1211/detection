import os
from os.path import splitext
from gensim.models import Word2Vec
import plyj.parser

from verbose_tools import get_classes
from verbose_tools import get_classes_properties
from extract_features_callgraph import extract_features


output_root = 'output'
input_root = 'input'
extract_features(input_root, output_root, plyj.parser.Parser())

sentences = []
patterns = dict()

file_data_store = dict()

for root, dirs, files in os.walk(output_root):
    # If the are no files in the verbose root folder
    if files is None:
        print("No files found in input directory")
        exit()
    for f in files:
        path = os.path.join(root, f)
        if ".verbose" in f:
            file_data_store[f]=dict()
            proj_name = splitext(f)[0]
            file_data_store[f]["project_name"] = proj_name
            # Groups the ngrams by class
            class_dict = get_classes(path)
            class_properties = get_classes_properties(path)

            if class_dict is None:
                print("No classes or ngrams extracted from project file {0}".format(f))
                continue
            file_data_store[f]["class_dict"] = class_dict
            file_data_store[f]["class_properties"] = class_properties

            # Appends the list of ngrams (in lower case) to a list of sentences
            for class_name, ngrams in class_dict.items():
                ngram_for_class = [ngram.lower() for ngram in ngrams]
                sentences.append(ngram_for_class)
        os.remove(path)


NDIM = 100                 ## currently set to 100 may change it to other value.
ngram_model = Word2Vec(sentences, vector_size=NDIM, window=20, min_count=2, workers=4)

saved_items_list = set()
saved_items_dicts = list()

for f,verbose_data in file_data_store.items():
    proj_name = verbose_data["project_name"]  # Name of the current project
    # Retrieve a dictionary constaining class names and corresponding ngrams
    class_dict = verbose_data["class_dict"]
    class_properties = verbose_data["class_properties"]
    # Iterate over class names and ngrams from the verbose file
    for class_name, ngrams in class_dict.items():

        # The if-block below makes sure that we only keep the labelled datasets in java_class_features.txt
        # This reduces size of java_class_features.txt, before this java_class_features.txt was almost 100 MB

        vector_ngram = [0.0 for _ in range(NDIM)]
        ngram_count = 0
        for ngram in ngrams:
            try:
                # TODO: Check if this line works as expected
                vector_ngram += ngram_model.wv[ngram.lower()]
                ngram_count += 1
            except Exception as e:
                # log.warning("Loading Word2Vec: {0}".format(e))
                pass


        # if any ngrams were present in the trained Word2Vec embedding model
        if ngram_count > 0:
            # Normalise the vector
            vector_ngram /= float(ngram_count)

        saved_items_list.add((proj_name,class_name))
        feature_dict = dict(project_name=proj_name,class_name=class_name,)
        class_properties[class_name].pop('method_return', None)
        class_properties[class_name].pop('class_name_words', None)
        feature_dict.update(class_properties[class_name])
        feature_dict.update({"w2v_"+str(i):x for i,x in enumerate(vector_ngram)})
        saved_items_dicts.append(feature_dict)

import pandas as pd

test = pd.DataFrame.from_records(saved_items_dicts)

data_file = "dataset.old.csv"

# Read data from a csv file into a pandas dataframe
data = pd.read_csv(data_file)

from sklearn.preprocessing import LabelEncoder

def preprocess_data(data: pd.DataFrame):
    data["implements"] = data["implements"].fillna(False)
    vc = data['pattern'].value_counts()
    vc = vc[vc>=10]
    labels_keep = set(vc.index)
    # print("Shape before filtering of data: ",data.shape)
    data = data[data['pattern'].isin(labels_keep)]
    data = data.sample(frac=1)
    # print("Shape after filtering of data: ",data.shape)

    return data

def extract_data(data: pd.DataFrame, drop_columns: list):
    y = data['pattern'] if 'pattern' in data.columns else None
    X = data.drop([drop for drop in drop_columns if drop in data.columns], axis=1)
    X.fillna(0,inplace=True)
    return X, y


from sklearn.svm import SVC
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression, RidgeClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier,VotingClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import ExtraTreesClassifier
import pickle


def random_forest():
    return RandomForestClassifier(n_estimators=1000)

def gb_tree():
    return GradientBoostingClassifier(n_estimators=100,max_depth=8,subsample=0.7,min_samples_leaf=4)

def adaboost_tree():
    return AdaBoostClassifier(estimator=RandomForestClassifier(n_estimators=100),n_estimators=20) # removed learning rate = 0.5
    
def adaboost_logistic():
    return AdaBoostClassifier(estimator=LogisticRegression(solver='lbfgs',class_weight="balanced",penalty="l2",multi_class="auto",n_jobs=-1,max_iter=500),
                              n_estimators=10,learning_rate=0.5)

def logistic():
    return make_pipeline(StandardScaler(),LogisticRegression(solver='saga',class_weight="balanced",penalty="l1",multi_class="auto",n_jobs=-1))

def ridge():
    return RidgeClassifier(alpha=0.3,normalize=True,class_weight="balanced")

def svm():
    return make_pipeline(StandardScaler(),SVC(class_weight="balanced",gamma='scale',C=1000.0,kernel="rbf",max_iter=100000,probability=True))

def voter():
    return VotingClassifier(estimators=[("svm",svm()),("rf",random_forest()),("adaboost",adaboost_tree())],
                            voting="soft")

def extra_trees():
    return ExtraTreesClassifier(bootstrap=False, criterion="gini", max_features=0.35000000000000003, min_samples_leaf=11, min_samples_split=15, n_estimators=100)

def save_model(model, fileName = 'model.pkl'):
    with open(fileName,'wb') as f:
        pickle.dump(model,f)

def load_model(fileName = 'model.pkl'):
    model = None
    with open(fileName, 'rb') as f:
        model = pickle.load(f)
    return model

def train_model(X_train, y_train):
    model = logistic()
    model.fit(X_train, y_train) #.astype(float)
    return model

def test_model(model, test, drop_columns, label_lookup):
    test["implements"] = test["implements"].fillna(False)
    X_test, _ = extract_data(test, drop_columns)
    cols_ordered = X_test.columns.tolist()
    cols_ordered.sort()
    X_test = X_test[cols_ordered]

    # print(X_test.columns)

    y_pred = model.predict(X_test)
    X_len = len(X_test)
    result = dict()
    for i in range(X_len):
        project_name = test['project_name'][i]
        if not result.get(project_name, False):
            result[project_name] = dict()
        class_name = test['class_name'][i]
        result[project_name][class_name] = label_lookup[y_pred[i]]
        # print(project_name, '\t\t', test['class_name'][i], '\t\t', label_lookup[y_pred[i]])
    return result


#############


drop_columns = ['project_name','class_name', 'pattern',
                'class_implements_last_name','class_last_name',
                'class_last_name_is_different', 'implements_name']

data = preprocess_data(data)
le = LabelEncoder()

data.pattern = le.fit_transform(data.pattern) # data.pattern = data['pattern']
label_lookup = le.classes_

fileName = 'model.pkl'
model = None
if (os.path.isfile(fileName)):
    model = load_model(fileName)
else:
    X_train, y_train = extract_data(data, drop_columns)
    cols_ordered = X_train.columns.tolist()
    cols_ordered.sort()
    X_train = X_train[cols_ordered]
    model = train_model(X_train, y_train)
    # print(X_train.columns)
    save_model(model, fileName)

result = test_model(model, test, drop_columns, label_lookup)

import json

json_object = json.dumps(result, indent=2)
 
with open(output_root + "/result.json", "w") as outfile:
    outfile.write(json_object)
